import express from 'express';
import json from 'body-parser';

export const router = express.Router();


//Declarar primer ruta por omision
router.get('/', (req, res) => {

    res.render('index', { titulo: "Mis practicas", nombre: "Mario Tirado" })
    //res.send(" <center> <h1>Alumno: </h1> <h1> Luis Mario Tirado Rios </h1> </center> ");
})

router.get('/tabla', (req, res) => {

    //parametros para poder tomar valores que manda la tbla
    const params = {
        numero: req.query.numero

    }
    res.render('tabla', params);



})


router.post('/tabla', (req, res) => {

    //parametros para poder tomar valores que manda la tbla
    const params = {
        numero: req.body.numero

    }
    res.render('tabla', params);



})









export default { router }